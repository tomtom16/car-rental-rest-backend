package at.ac.fhcampuswien.se.car.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import at.ac.fhcampuswien.se.car.rest.applicaton.RestApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestApplication.class})
public class RestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
