package at.ac.fhcampuswien.se.car.rest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "\"Order\"")
public class Order implements Serializable {

	private static final long serialVersionUID = 2013562696581626681L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String status;

	@Column(nullable = false)
	private Date fromDate;

	@Column(nullable = false)
	private Date toDate;

	@ManyToOne
	private Location location;

	@ManyToOne
	@JsonBackReference
	private User user;

	@ManyToOne
	private Category category;

	@Column(nullable = false)
	private boolean includesInsurance;

	@Column(nullable = false)
	private float price;

	@Column(nullable = false)
	private String currency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public boolean isIncludesInsurance() {
		return includesInsurance;
	}

	public void setIncludesInsurance(boolean includesInsurance) {
		this.includesInsurance = includesInsurance;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
