package at.ac.fhcampuswien.se.car.rest.controller;

import at.ac.fhcampuswien.se.car.rest.model.User;
import at.ac.fhcampuswien.se.car.rest.repository.UserRepository;
import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/register")
class RegisterController {

	@Resource
	private UserRepository userRepository;

	@Resource
	private PasswordEncoder passwordEncoder;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	User registerNewUser(@RequestBody User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return userRepository.save(user);
	}

	@GetMapping
	List<User> getAllUsers() {
		return Lists.newArrayList(userRepository.findAll());
	}
}
