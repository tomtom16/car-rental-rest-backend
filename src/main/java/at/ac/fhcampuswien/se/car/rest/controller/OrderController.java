package at.ac.fhcampuswien.se.car.rest.controller;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import at.ac.fhcampuswien.se.car.rest.model.Order;
import at.ac.fhcampuswien.se.car.rest.model.User;
import at.ac.fhcampuswien.se.car.rest.repository.CategoryRepository;
import at.ac.fhcampuswien.se.car.rest.repository.LocationRepository;
import at.ac.fhcampuswien.se.car.rest.repository.OrderRepository;
import at.ac.fhcampuswien.se.car.rest.service.ServiceImpl;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Resource
	private CategoryRepository categoryRepository;
	
	@Resource
	private OrderRepository orderRepository;
	
	@Resource
	private LocationRepository locationRepository;
	
    @Resource
    private ServiceImpl service;
    
    @GetMapping
    List<Order> getAllAvailableCategories() {
        
    	List<Order> orders = service.findAllOrders();
    	
    	return orders;
    }
    
    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    Set<Order> getOrdersForUser(@PathVariable("username") String username)
    {
    	User user = service.getUser(username);
    	
    	return user.getOrders();
    }
    
    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	Order createOrder(@RequestBody Order order) {
		
    	Long locationId = order.getLocation().getId();
    	Long categoryId = order.getCategory().getId();
    	
    	order.setCategory(categoryRepository.findById(categoryId).get());
    	order.setLocation(locationRepository.findById(locationId).get());
    	order.setUser(service.getCurrentUser());
    	
    	service.calculate(order);
    	
    	orderRepository.save(order);
    	
    	return order;
	}
}
