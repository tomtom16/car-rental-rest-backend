package at.ac.fhcampuswien.se.car.rest.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import at.ac.fhcampuswien.se.car.rest.soap.CurrencyRequest;
import at.ac.fhcampuswien.se.car.rest.soap.ExchangeCurrencyRequest;
import at.ac.fhcampuswien.se.car.rest.soap.ExchangeCurrencyResponse;

public class CurrencySoapClient extends WebServiceGatewaySupport {

	@Value("${soap.server.endpoint}")
	private String soapServerEndpoint;

	
	public ExchangeCurrencyResponse getExchangeRate(String fromCurrency, float fromCurrencyAmount, String toCurrency) {
		ExchangeCurrencyRequest exchangeCurrencyRequest = new ExchangeCurrencyRequest();

		CurrencyRequest currencyRequest = new CurrencyRequest();
		currencyRequest.setFromCurrency(fromCurrency);
		currencyRequest.setFromCurrencyAmount(fromCurrencyAmount);
		currencyRequest.setToCurrency(toCurrency);

		exchangeCurrencyRequest.setRequest(currencyRequest);
		ExchangeCurrencyResponse response = (ExchangeCurrencyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(
						soapServerEndpoint,
						exchangeCurrencyRequest,
						new SoapActionCallback("http://spring.io/guides/gs-producing-web-service/ExchangeCurrencyRequest")
				);

		return response;
	}

}