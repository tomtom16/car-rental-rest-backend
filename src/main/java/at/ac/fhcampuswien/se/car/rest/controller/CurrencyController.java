package at.ac.fhcampuswien.se.car.rest.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import at.ac.fhcampuswien.se.car.rest.service.CurrencySoapClient;
import at.ac.fhcampuswien.se.car.rest.soap.ExchangeCurrencyResponse;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

	@Resource
	private CurrencySoapClient currencySoapClient;
	
	@RequestMapping(value = "/{fromCurrency}/{toCurrency}/{amount}", method = RequestMethod.GET)
	public String getCurrency(@PathVariable("fromCurrency") String fromCurrency, @PathVariable("toCurrency") String toCurrency, @PathVariable("amount") float amount) {
		ExchangeCurrencyResponse resp = currencySoapClient.getExchangeRate(fromCurrency, amount, toCurrency);
		return resp.getResponse().getToCurrency() + " " + resp.getResponse().getToCurrencyAmount();
	}
	
}
