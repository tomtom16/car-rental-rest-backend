package at.ac.fhcampuswien.se.car.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/user")
    public ResponseEntity userLoggedIn() {
        return ResponseEntity.ok().build();
    }
}
