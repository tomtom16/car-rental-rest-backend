package at.ac.fhcampuswien.se.car.rest.enums;

public enum BookingState {
    AVAILABLE, LOCKED, BOOKED
}
