package at.ac.fhcampuswien.se.car.rest.applicaton;

import java.util.List;

import javax.annotation.Resource;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;

import at.ac.fhcampuswien.se.car.rest.model.Category;
import at.ac.fhcampuswien.se.car.rest.model.Location;
import at.ac.fhcampuswien.se.car.rest.model.Order;
import at.ac.fhcampuswien.se.car.rest.model.User;
import at.ac.fhcampuswien.se.car.rest.repository.CategoryRepository;
import at.ac.fhcampuswien.se.car.rest.repository.LocationRepository;
import at.ac.fhcampuswien.se.car.rest.repository.OrderRepository;
import at.ac.fhcampuswien.se.car.rest.repository.UserRepository;

@Configuration
class DatabaseLoader {
	
	@Resource
	PasswordEncoder passwordEncoder;

	@Bean
	CommandLineRunner initDatabase(CategoryRepository categoryRepository, UserRepository userRepository, LocationRepository locationRepository, OrderRepository orderRepository) {
		return args -> {
			createDummyCategories(categoryRepository);
			createLocations(locationRepository);
			createDummyUser(userRepository);
			createDummyOrder(categoryRepository, userRepository, locationRepository, orderRepository);
		};
	}

	private void createDummyOrder(CategoryRepository categoryRepository, UserRepository userRepository, LocationRepository locationRepository, OrderRepository orderRepository) {
		Order order = new Order();
		
		order.setCategory(categoryRepository.findAll().iterator().next());
		
		LocalDate fromDate = new LocalDate(2019, 3, 1);
		LocalDate toDate = new LocalDate(2019, 3, 5);
		
		order.setFromDate(fromDate.toDate());
		order.setToDate(toDate.toDate());
		order.setIncludesInsurance(true);
		
		int days = Days.daysBetween(LocalDate.fromDateFields(order.getFromDate()), LocalDate.fromDateFields(order.getToDate())).getDays();
		float insuancePrice = order.isIncludesInsurance() ? order.getCategory().getInsurancePrice() : 0f;
		order.setPrice(order.getCategory().getPrice() * days + insuancePrice);
		
		List<Location> locations = Lists.newArrayList(locationRepository.findAll());
		order.setLocation(locations.get(0));
		
		order.setStatus("CONFIRMED");
		
		order.setUser(userRepository.findAll().iterator().next());
		
		order.setCurrency(order.getCategory().getCurrency());
		
		orderRepository.save(order);
		
	}

	private void createLocations(LocationRepository locationRepository) {
		Location vienna = new Location();
		vienna.setCity("Vienna");
		vienna.setCountry("Austria");
		vienna.setLatitude(48.208176f);
		vienna.setLongitude(16.373819f);
		locationRepository.save(vienna);
		
		Location salzburg = new Location();
		salzburg.setCity("Salzburg");
		salzburg.setCountry("Austria");
		salzburg.setLatitude(47.809490f);
		salzburg.setLongitude(13.055010f);
		locationRepository.save(salzburg);
		
		Location graz = new Location();
		graz.setCity("Graz");
		graz.setCountry("Austria");
		graz.setLatitude(47.070713f);
		graz.setLongitude(15.439504f);
		locationRepository.save(graz);
		
		Location innsbruck = new Location();
		innsbruck.setCity("Innsbruck");
		innsbruck.setCountry("Austria");
		innsbruck.setLatitude(47.262691f);
		innsbruck.setLongitude(11.394700f);
		locationRepository.save(innsbruck);
		
		Location klagenfurt = new Location();
		klagenfurt.setCity("Klagenfurt");
		klagenfurt.setCountry("Austria");
		klagenfurt.setLatitude(46.623669f);
		klagenfurt.setLongitude(14.307590f);
		locationRepository.save(klagenfurt);
		
		Location zurich = new Location();
		zurich.setCity("Zürich");
		zurich.setCountry("Switzerland");
		zurich.setLatitude(47.376888f);
		zurich.setLongitude(8.541694f);
		locationRepository.save(zurich);
		
		Location bern = new Location();
		bern.setCity("Bern");
		bern.setCountry("Switzerland");
		bern.setLatitude(46.947975f);
		bern.setLongitude(7.447447f);
		locationRepository.save(bern);
		
		Location berlin = new Location();
		berlin.setCity("Berlin");
		berlin.setCountry("Germany");
		berlin.setLatitude(52.520008f);
		berlin.setLongitude(13.404954f);
		locationRepository.save(berlin);
		
		Location hamburg = new Location();
		hamburg.setCity("Hamburg");
		hamburg.setCountry("Germany");
		hamburg.setLatitude(53.551086f);
		hamburg.setLongitude(9.993682f);
		locationRepository.save(hamburg);
		
		Location munich = new Location();
		munich.setCity("Munich");
		munich.setCountry("Germany");
		munich.setLatitude(48.135124f);
		munich.setLongitude(11.581981f);
		locationRepository.save(munich);
	}

	private void createDummyCategories(CategoryRepository categoryRepository) {
		Category economy = new Category();
		economy.setName("Economy");
		economy.setPrice(55f);
		economy.setInsurancePrice(15f);
		economy.setCurrency("USD");
		categoryRepository.save(economy);

		Category compact = new Category();
		compact.setName("Compact");
		compact.setPrice(65f);
		compact.setInsurancePrice(18f);
		compact.setCurrency("USD");
		categoryRepository.save(compact);
		
		Category intermediate = new Category();
		intermediate.setName("Intermediate");
		intermediate.setPrice(85f);
		intermediate.setInsurancePrice(23f);
		intermediate.setCurrency("USD");
		categoryRepository.save(intermediate);
		
		Category fullsize = new Category();
		fullsize.setName("Full Size");
		fullsize.setPrice(110f);
		fullsize.setInsurancePrice(28f);
		fullsize.setCurrency("USD");
		categoryRepository.save(fullsize);
	}

	private void createDummyUser(UserRepository userRepository) {
		User admin = new User();
		admin.setUsername("admin");
		admin.setEmail("admin@carbooking.com");
		admin.setEnabled(true);
		admin.setFirstName("Administrator");
		admin.setLastName("Carbooking");
		admin.setPassword(passwordEncoder.encode("nimda"));
		userRepository.save(admin);
		
		User user = new User();
		user.setUsername("user");
		user.setEmail("user@carbooking.com");
		user.setEnabled(true);
		user.setFirstName("User");
		user.setLastName("Carbooking");
		user.setPassword(passwordEncoder.encode("user"));
		userRepository.save(user);
	}
}
