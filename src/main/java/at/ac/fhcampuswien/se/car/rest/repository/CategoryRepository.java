package at.ac.fhcampuswien.se.car.rest.repository;

import org.springframework.data.repository.CrudRepository;

import at.ac.fhcampuswien.se.car.rest.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
