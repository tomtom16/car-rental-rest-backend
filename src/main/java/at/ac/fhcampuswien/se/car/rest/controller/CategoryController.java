package at.ac.fhcampuswien.se.car.rest.controller;

import at.ac.fhcampuswien.se.car.rest.model.Category;
import at.ac.fhcampuswien.se.car.rest.service.ServiceImpl;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Resource
    private ServiceImpl service;
    
    @RequestMapping(value = "/{toCurrency}", method = RequestMethod.GET)
    List<Category> getAllAvailableCategories(@PathVariable("toCurrency") String toCurrency) {
        
    	return service.getAllAvailableCategories(toCurrency);
    }
}
