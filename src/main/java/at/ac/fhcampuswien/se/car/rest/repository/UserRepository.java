package at.ac.fhcampuswien.se.car.rest.repository;

import org.springframework.data.repository.CrudRepository;

import at.ac.fhcampuswien.se.car.rest.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	public User findByUsername(String username);
}
