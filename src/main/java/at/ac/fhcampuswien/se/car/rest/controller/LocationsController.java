package at.ac.fhcampuswien.se.car.rest.controller;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import at.ac.fhcampuswien.se.car.rest.model.Location;
import at.ac.fhcampuswien.se.car.rest.service.CurrencySoapClient;
import at.ac.fhcampuswien.se.car.rest.service.ServiceImpl;

@RestController
@RequestMapping("/locations")
public class LocationsController {

    @Resource
    private ServiceImpl service;
    
    @Resource
	private CurrencySoapClient currencySoapClient;

    @GetMapping
    List<Location> getAllLocations() {
        
    	List<Location> locations = service.getAllLocations();
    	
    	return locations;
    }
    
    @RequestMapping(value = "/{country}", method = RequestMethod.GET)
    List<Location> getLocationsForCountry(@PathVariable("country") String country) {
    	
    	List<Location> locations = service.getLocationsByCountry(country);
    	
    	return locations;
    }
    
    @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = "application/json")
    String getCountries() {
    	List<Location> locations = service.getAllLocations().stream().sorted(Comparator.comparing(Location::getCountry)).collect(Collectors.toList());
    	Set<String> countries = new LinkedHashSet<>();
    	countries.addAll(locations.stream().map(Location::getCountry).collect(Collectors.toList()));
    	
    	Map<String, Object> data = new HashMap<>();
    	data.put("countries", countries);
    	
    	Gson gson = new Gson();
    	String json = gson.toJson(data); 
    	
    	return json;
    }
    
}
