package at.ac.fhcampuswien.se.car.rest.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import at.ac.fhcampuswien.se.car.rest.model.User;
import at.ac.fhcampuswien.se.car.rest.repository.UserRepository;

@Service("restUserDetailsService")
@Transactional
public class RestUserDetailsService implements UserDetailsService {

	private static final String ROLE_PREFIX = "ROLE_";
	
	@Autowired
    private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
        if (user == null) {
            return new org.springframework.security.core.userdetails.User(
              " ", " ", true, true, true, true, 
              getAnonymousAuthorities());
        }
 
        return new org.springframework.security.core.userdetails.User(
          user.getUsername(), user.getPassword(), user.isEnabled(), true, true, 
          true, getUserAuthorities());

	}
	
	private List<GrantedAuthority> getAnonymousAuthorities() 
	{
		List<GrantedAuthority> authorities = Lists.newArrayList();
		
		authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + "ANONYMOUS"));
		
		return authorities;
	}
	
	private List<GrantedAuthority> getUserAuthorities()
	{
		List<GrantedAuthority> authorities = Lists.newArrayList();
		
		authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + "USER"));
		
		return authorities;
	}

	
}
