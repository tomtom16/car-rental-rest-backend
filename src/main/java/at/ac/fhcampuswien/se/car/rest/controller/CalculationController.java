package at.ac.fhcampuswien.se.car.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import at.ac.fhcampuswien.se.car.rest.model.Category;
import at.ac.fhcampuswien.se.car.rest.model.Location;
import at.ac.fhcampuswien.se.car.rest.model.Order;
import at.ac.fhcampuswien.se.car.rest.repository.CategoryRepository;
import at.ac.fhcampuswien.se.car.rest.repository.LocationRepository;
import at.ac.fhcampuswien.se.car.rest.service.ServiceImpl;

@RestController
@RequestMapping("/calculate")
public class CalculationController {

	@Resource
	private CategoryRepository categoryRepository;
	
	@Resource
	private LocationRepository locationRepository;
	
    @Resource
    private ServiceImpl service;
    
    @PostMapping
	@ResponseStatus(HttpStatus.OK)
	Order calculate(@RequestBody Order order) {
		
    	Long locationId = order.getLocation().getId();
    	Long categoryId = order.getCategory().getId();
    	
    	order.setCategory(categoryRepository.findById(categoryId).get());
    	order.setLocation(locationRepository.findById(locationId).get());
    	
    	service.calculate(order);
    	
    	return order;
	}
    
    @RequestMapping(value = "/categories", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	List<Order> calculateCategories(@RequestBody Order order) {
		
    	Long locationId = order.getLocation().getId();
    	Location loc = locationRepository.findById(locationId).get();
    	List<Category> categories = service.getAllAvailableCategories();
    	List<Order> orders = new ArrayList<>();
    	
    	for (Category category : categories)
    	{
    		Order ret = new Order();
    		
    		ret.setCategory(category);
    		ret.setLocation(loc);
    		ret.setFromDate(order.getFromDate());
    		ret.setToDate(order.getToDate());
    		ret.setIncludesInsurance(order.isIncludesInsurance());
    		ret.setCurrency(order.getCurrency());
    		
    		service.calculate(ret);
    		orders.add(ret);
    	}
    	
    	
    	return orders;
	}
}
