package at.ac.fhcampuswien.se.car.rest.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import at.ac.fhcampuswien.se.car.rest.model.Location;

public interface LocationRepository extends CrudRepository<Location, Long> {
	List<Location> findByCountry(String country);
}
