package at.ac.fhcampuswien.se.car.rest.applicaton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ServletComponentScan
@ComponentScan("at.ac.fhcampuswien.se")
@EntityScan("at.ac.fhcampuswien.se.car.rest.model")
@EnableJpaRepositories("at.ac.fhcampuswien.se.car.rest.repository")
@SpringBootApplication
public class RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApplication.class, args);
	}

//	@Bean
//	CommandLineRunner lookup(CurrencySoapClient currencyClient) {
//		return args -> {
//			ExchangeCurrencyResponse response = currencyClient.getExchangeRate("USD", 100, "EUR");
//			System.out.println(response.getResponse().getToCurrency() + " " + response.getResponse().getToCurrencyAmount());
//		};
//	}
}
