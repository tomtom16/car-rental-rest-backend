package at.ac.fhcampuswien.se.car.rest.applicaton;

import at.ac.fhcampuswien.se.car.rest.service.CurrencySoapClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;

@Configuration
public class SoapCurrencyConfiguration {

	@Value("${soap.server.endpoint}")
	private String soapServerEndpoint;
	
    @Bean
    public Jaxb2Marshaller marshallerCurrency() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("at.ac.fhcampuswien.se.car.rest.soap");
        return marshaller;
    }

    @Bean
    public CurrencySoapClient currencyClient(Jaxb2Marshaller marshallerCurrency) {
        System.out.println("SOAP SERVER ENDPOINT : " + soapServerEndpoint);
    	
    	CurrencySoapClient client = new CurrencySoapClient();
        client.setDefaultUri(soapServerEndpoint);
        client.setMarshaller(marshallerCurrency);
        client.setUnmarshaller(marshallerCurrency);
        ClientInterceptor[] interceptors = new ClientInterceptor[] { securityInterceptor() };
        client.setInterceptors(interceptors);
        return client;
    }

    @Bean
    public Wss4jSecurityInterceptor securityInterceptor(){
        Wss4jSecurityInterceptor wss4jSecurityInterceptor = new Wss4jSecurityInterceptor();
        wss4jSecurityInterceptor.setSecurementActions("Timestamp UsernameToken");
        wss4jSecurityInterceptor.setSecurementUsername("super-secure-user");
        wss4jSecurityInterceptor.setSecurementPassword("secure-soap-password");
        wss4jSecurityInterceptor.setSecurementPasswordType("PasswordText");
        // <property name="securementUsernameTokenElements" value="Nonce Created"/>
        wss4jSecurityInterceptor.setSecurementUsernameTokenCreated(true);
        wss4jSecurityInterceptor.setSecurementUsernameTokenNonce(true);
        return wss4jSecurityInterceptor;
    }

}
