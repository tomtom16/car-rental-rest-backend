package at.ac.fhcampuswien.se.car.rest.applicaton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import at.ac.fhcampuswien.se.car.rest.soap.ExchangePortService;
import at.ac.fhcampuswien.se.car.rest.soap.ObjectFactory;

@Configuration
public class CurrencyConfiguration {
    
	private ExchangePortService exchangePortService;
	
	private ObjectFactory objectFactory;
	
	@Bean
    public ExchangePortService exchangePortService()
    {
    	if (exchangePortService == null)
    	{
    		exchangePortService = new ExchangePortService();
    	}
    	return exchangePortService;
    }
	
	@Bean
	public ObjectFactory objectFactory()
	{
		if (objectFactory == null)
		{
			objectFactory = new ObjectFactory();
		}
		return objectFactory;
	}
}