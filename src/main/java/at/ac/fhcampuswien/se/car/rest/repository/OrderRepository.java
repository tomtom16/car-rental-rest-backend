package at.ac.fhcampuswien.se.car.rest.repository;

import org.springframework.data.repository.CrudRepository;

import at.ac.fhcampuswien.se.car.rest.model.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
