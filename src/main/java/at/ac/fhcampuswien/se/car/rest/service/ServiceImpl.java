package at.ac.fhcampuswien.se.car.rest.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import at.ac.fhcampuswien.se.car.rest.model.Category;
import at.ac.fhcampuswien.se.car.rest.model.Location;
import at.ac.fhcampuswien.se.car.rest.model.Order;
import at.ac.fhcampuswien.se.car.rest.model.User;
import at.ac.fhcampuswien.se.car.rest.repository.CategoryRepository;
import at.ac.fhcampuswien.se.car.rest.repository.LocationRepository;
import at.ac.fhcampuswien.se.car.rest.repository.OrderRepository;
import at.ac.fhcampuswien.se.car.rest.repository.UserRepository;
import at.ac.fhcampuswien.se.car.rest.soap.ExchangeCurrencyResponse;

@Service
public class ServiceImpl {

    @Resource
    private UserRepository userRepository;

    @Resource
    private CategoryRepository categoryRepository;
    
    @Resource
    private LocationRepository locationRepository;
    
    @Resource
    private OrderRepository orderRepository;
    
    @Autowired
    private CurrencySoapClient currencyClient;
    
    @Resource
    private PasswordEncoder passwordEncoder;
    
    private static final String[] ORDER_STATUS = {"CONFIRMED", "CANCELED"};
    
    public List<Category> getAllAvailableCategories() {
        return  Lists.newArrayList(categoryRepository.findAll());
    }
    
    public User getUser(String username)
    {
    	return userRepository.findByUsername(username);
    }
    
    public List<Category> getAllAvailableCategories(String toCurrency)
    {
    	List<Category> allAvailableCategories = Lists.newArrayList(categoryRepository.findAll());
    	
    	List<Category> ret = Lists.newArrayList();
    	
    	for (Category category : allAvailableCategories)
    	{
			ret.add(prepareCategoryForCurrency(category, toCurrency));
    	}
    	
        return ret;
    }
    
    public Category prepareCategoryForCurrency(Category category, String toCurrency)
    {
    	if (!StringUtils.equals(category.getCurrency(), toCurrency)) 
    	{
    		ExchangeCurrencyResponse resp = currencyClient.getExchangeRate(category.getCurrency(), category.getPrice(), toCurrency);
    		category.setPrice(resp.getResponse().getToCurrencyAmount());
    		
    		ExchangeCurrencyResponse resp2 = currencyClient.getExchangeRate(category.getCurrency(), category.getInsurancePrice(), toCurrency);
    		category.setInsurancePrice(resp2.getResponse().getToCurrencyAmount());
    		
    		category.setCurrency(resp.getResponse().getToCurrency());
    	}
		
		return category;
    }
    
    public List<Location> getAllLocations()
    {
    	List<Location> allLocations = Lists.newArrayList(locationRepository.findAll());
    	return allLocations;
    }
    
    public List<Location> getLocationsByCountry(String country)
    {
    	List<Location> locations = Lists.newArrayList(locationRepository.findByCountry(country));
    	return locations;
    }
    
    public List<Order> findAllOrders()
    {
    	List<Order> orders = Lists.newArrayList(orderRepository.findAll());
    	return orders;
    }
    
    public void calculate(Order order) 
    {
    	
		order.setStatus(ORDER_STATUS[0]);
    	
    	String targetCurrency = order.getCurrency();

    	order.setCategory(prepareCategoryForCurrency(order.getCategory(), targetCurrency));
    	
    	int days = Days.daysBetween(LocalDate.fromDateFields(order.getFromDate()), LocalDate.fromDateFields(order.getToDate())).getDays();
		float insuancePrice = order.isIncludesInsurance() ? order.getCategory().getInsurancePrice() : 0f;
    	
		float price =  order.getCategory().getPrice() * days + insuancePrice * days;
    	
    	order.setPrice(price);
    }
    
	public User getCurrentUser()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); 
		return userRepository.findByUsername(username);
	}
	
//    public List<Car> getAllAssignedCarsForUser(Long userId) {
//        return userRepository.findById(userId).get().getCars();
//    }

//    public void bookCar(long userId, Long carId) {
//        if (!getAllAssignedCarsForUser(userId).contains(getCar(carId))) {
//            getAllAssignedCarsForUser(userId).add(getCar(carId));
//        }
//    }

//    private void setBookingState(Long carId, BookingState bookingState) {
//        getCar(carId).setBookingState(bookingState);
//    }

//    private Category getCategory(Long id) {
//        return categoryRepository.findById(id).get();
//    }
}
